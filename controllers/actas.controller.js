'use strict'
let fs = require('fs');
let path = require('path');

// function obtenerArchivo(req, res) {

//     let imagen_file = req.query.archivo;    
//     let path_file = './uploads/' + imagen_file;
//     if (path_file.length > 0) {        
//         fs.stat(path_file, function (error, exists) {            
//             if (exists) {
//                 console.log("--------------------");
//                 console.log("Archivo Solicitado: ", path_file);
//                 console.log("--------------------");
//                 if (imagen_file.includes(".tar.gz") || imagen_file.includes(".zip") ) {
//                     res.download(path.resolve(path_file), imagen_file);
//                 }
//                 else if(imagen_file.includes(".jpg")) {
//                     res.sendFile(path.resolve(path_file));
//                 } else{
//                     res.status(200).send({ message: 'Acceso denegado' });    
//                 }
//             } else {
//                 res.status(200).send({ message: 'No existe la imagen' });
//             }
//         });
//     } else {
//         res.status(200).send({ message: 'No solicitó un archivo' });
//     }
// }

function obtenerArchivo(req, res) {
    // console.log("obtenerImagen -> req.params =>", req.params);
    // let imagen_file = req.params.archivo;
    console.log("obtenerImagen -> req.query =>", req.query);
    let imagen_file = req.query.archivo;
    let path_file = './uploads/' + imagen_file;
    if (path_file.length > 0) {        
        fs.stat(path_file, (error, stats) => { 
            if (error) {
                res.status(200).send({ message: 'No existe la imagen' });
            } else {                
                console.log("--------------------");
                console.log("Archivo Solicitado: ", path_file);
                console.log("--------------------");
                if (imagen_file.includes(".tar.gz") || imagen_file.includes(".zip") ) {
                    res.download(path.resolve(path_file), imagen_file);
                }
                else if(imagen_file.includes(".jpg")) {
                    res.sendFile(path.resolve(path_file));
                } else{
                    res.status(200).send({ message: 'Acceso denegado' });    
                }

            }
        });
    } else {
        res.status(200).send({ message: 'No solicitó un archivo' });
    }
}

function status(req, res){
    res.status(200).send({message: 'ok'});
}

module.exports = {
    obtenerArchivo,
    status
}